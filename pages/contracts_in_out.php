
						<div class="row">
							<div class="col-md-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													ΦΙΛΤΡΑ
												</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body">
                                      
                                        <div class="form-group m-form__group row">
												<div class="col-lg-4">
													<label>
														ΑΝΑΖΗΤΗΣΗ
													</label>
													<div class="m-input-icon m-input-icon--left">
														<input type="text" class="form-control m-input" placeholder="Phone number">
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                                    </div>
												</div>
												<div class="col-lg-3">
                                                    <label>
                                                       ΗΜΕΡ/ΝΙΑ ΑΦΙΞΗΣ
                                                    </label>
                                                   
                                                        <div class="m-input-icon m-input-icon--left">
                                                            <input type="text" class="form-control m-input" readonly  placeholder="Select date" id="m_datepicker_2"/>
                                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                                <span>
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </span>
                                        
                                                    </div>
												</div>
												<div class="col-lg-3">
                                                    <label>
                                                       ΗΜΕΡ/ΝΙΑ ΑΝΑΧΩΡΗΣΗΣ
                                                    </label>
                                                   
                                                        <div class="m-input-icon m-input-icon--left">
                                                            <input type="text" class="form-control m-input" readonly  placeholder="Select date" id="m_datepicker_2"/>
                                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                                <span>
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </span>
                                        
														</div>
												</div>
												<div class="col-lg-2" style="padding-top:2%">
													<a href="#" class="btn btn-primary m-btn m-btn--sm "  style="font-size: 13px">
													<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Αναζήτηση&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>					
													</a>
												</div>
											</div>



                                    </div>
                                </div>
							</div>
						</div>


																
								

								<div class="row">
										<div class="col-md-12">
											<!--begin::Portlet-->
											<div class="m-portlet m-portlet--tabs  m-portlet--head-solid-bg m-portlet--bordered">
													<div class="m-portlet__head">
														<div class="m-portlet__head-caption">
															
														</div>
														<div class="m-portlet__head-tools">
																<ul class="m-portlet__nav">
																	<li class="m-portlet__nav-item">
																	<a class="btn btn-info m-btn m-btn--custom m-btn--icon"  data-toggle="modal" data-target="#m_modal_6" style='color:white'><span><i class="la la-plus-circle"></i><span style="font-size: 13px">	
																	<b>Προσθήκη Συμβολαίου</b>					
																	</span></span></a>
																	</li>
																</ul>
															</div>
														</div>
												<div class="m-portlet__body">
														<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style='font-family: Roboto Slab !important;'>
																<thead>
																	<tr>
																		<th>
																			RecordID
																		</th>
																		<th>
																			CompanyEmail
																		</th>
																		<th>
																			CompanyAgent
																		</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>
																			1
																		</td>
																		<td >
																			nsailor0@livejournal.com
																		</td>
																		<td>
																			Nixie Sailor
																		</td>
																		<td nowrap></td>
																	</tr>
																	<tr>
																		<td>
																			2
																		</td>
																		<td>
																			egiraldez1@seattletimes.com
																		</td>
																		<td>
																			Emelita Giraldez
																		</td>
																		<td nowrap></td>
																	</tr>
																	<tr>
																		<td>
																			3
																		</td>
																		<td>
																			uluckin2@state.gov
																		</td>
																		<td>
																			Ula Luckin
																		</td>
																		<td nowrap></td>
																	</tr>
																</tbody>
															</table>
			
			
			
												</div>
											</div>
										</div>
									<div>


<script>
	$(document).ready(function() {
    $('#m_table_1').DataTable( {
		"columnDefs": 
		[
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {

					var template_in_out = 
					"\
					<div class='row' style='padding-top: 1%;'>\
						<div class='col-md-4'>\
						</div>\
						<div class='col-md-8' style='padding-left: 22.5px; font-size: 26px;'>\
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
						</div>\
					</div>\
					<div class='row'>\
						<div class='col-md-4'>\
						</div>\
						<div class='col-md-8'>\
						<a class='btn btn-info m-btn m-btn--icon btn-lg m-btn--icon-only inactiveLink' style='color: white;'><i class='la la-car'></i></a>\
						&nbsp;&nbsp;<b>AZD2312</b>\
						</div>\
					</div>\
					<div class='row' style='padding-top: 7%;'>\
						<div class='col-md-4'>\
							<span class='dropdown dropdown-menu-left'>\
							<a href='#' class='btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill' data-toggle='dropdown' aria-expanded='false'>\
									<i class='la la-ellipsis-h'></i>\
								</a>\
                                <div class='dropdown-menu dropdown-menu-left ' x-placement='bottom-end' style='position: absolute;'>\
                                    <a class='dropdown-item' href='#'><i class='la la-stop'></i> Tερματισμός συμβολαίου</a>\
									<a class='dropdown-item' href='#'><i class='la la-edit'></i> Επεξεργασία</a>\
									<a class='dropdown-item' href='#'><i class='la la-trash'></i> Ακύρωση συμβολαίου</a>\
								</div>\
							</span>\
						</div>\
						<div class='col-md-8' style='padding-left: 22.5px; font-size: 14px;'>\
						<a class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill inactiveLink' style='width: 25px;height: 25px; background-color: #fff;'><i class='la la-user'></i></a>\
						Καραμίτσος αποστόλος\
						</div>\
					</div>\
					<div class='row' style='padding-top: 1%;'>\
						<div class='col-md-4'>\
						</div>\
						<div class='col-md-8' style='padding-left: 22.5px; font-size: 14px; margin-top: -8px;'>\
						<a class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill inactiveLink' style='width: 25px;height: 25px; background-color: #fff;'><i class='la la-phone'></i></a>\
						6986154125\
						</div>\
					</div>\
					<div class='row' style='padding-top: 1%;'>\
						<div class='col-md-4'>\
						</div>\
						<div class='col-md-8' style='padding-left: 22.5px; font-size: 14px;'>\
						<a class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill inactiveLink' style='width: 25px;height: 25px; background-color: #fff;'><i class='la la-users'></i></a>\
						4\
						</div>\
					</div>\
					<div class='row' style='padding-top: 1%;'>\
						<div class='col-md-4'>\
						</div>\
						<div class='col-md-8' style='padding-left: 22.5px; font-size: 26px;'>\
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
						</div>\
					</div>\
					\
					\
					\
					";
                    return template_in_out;
                },
                "targets": 0
            },
			{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {

					var template_in_out = 
					"\
					<div class='row'>\
						<div class='col-md-5 center-block text-center' style='center-block text-center'>\
							<div class='row' style='font-size: 13px; color: #929292; padding-left: 25%' >\
								<b>Ημερ/νια Δημιουργίας</b>\
							</div>\
							<div class='row'  style='padding-top: 3%; font-size: 20px; padding-left: 25%'>\
								<b>11:45</b>\
							</div>\
							<div class='row'  style='padding-top: 3%; font-size: 13px; color: #929292; padding-left: 25%''>\
								<b>22/12/2016</b>\
							</div>\
						</div>\
						<div class='col-md-2 center-block text-center' style='center-block text-center'>\
							<div style='border-left:2px solid #0000004d;height:95px'></div>\
						</div>\
						<div class='col-md-5 center-block text-center' style='center-block text-center'>\
							<div class='row' style='font-size: 13px; color: #929292;'>\
								<b>Eπόμενη ανανέωση</b>\
							</div>\
							<div class='row'  style='padding-top: 3%; font-size: 20px;  color: #929292;'>\
								22:00\
							</div>\
							<div class='row'  style='padding-top: 3%; font-size: 13px; color: #929292;'>\
								<b>26/12/2016</b>\
							</div>\
						</div>\
					</div>\
					\
					\
					\
					";
                    return template_in_out;
                },
                "targets": 1
            },
			{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {

					var template_in_out = 
					"\
						<div class='row'>\
							<div class='col-md-3'>\
                                <div class='row'  style='padding-left: 25%'>\
                                    <b style='color: #929292; font-size:13px;'>Επιπλέον οχήματα</b>\
                                    <div style='padding-top:10%;'>\
									<a  class='btn btn-outline-metal m-btn m-btn--icon inactiveLink'>\
                                        <span>\
                                            <i class='la la-car' style='color: #0000ffb3;'></i>\
                                            <span>\
                                                <b>EEE1332</b>\
                                            </span>\
                                        </span>\
                                    </a>\
                                    </div>\
                                    <div style='padding-top:4%;'>\
									<a  class='btn btn-outline-metal m-btn m-btn--icon inactiveLink'>\
                                        <span>\
                                            <i class='la la-car' style='color: #0000ffb3;'></i>\
                                            <span>\
                                                <b>EAY1302</b>\
                                            </span>\
                                        </span>\
                                    </a>\
                                    </div>\
                                </div>\
							</div>\
							<div class='col-md-1'>\
							</div>\
							<div class='col-md-8'>\
								<div class='row' style='padding-left: 25%'>\
								<a href='#' class='btn btn-success m-btn m-btn--custom m-btn--icon'><span><i class='la la-refresh'></i><span style='font-size: 16px'>\
								<b>Ανανέωση Συμβολαίου</b>\
								</span></span></a>\
								</div>\
								<div class='row' style='word-wrap: break-word; '>\
								<div class='col-md-12' style='padding-left: 0px !important; font-size:13px;'>\
								<hr>\
								<b style='color: #929292;'>Σχόλια :</b>\
								kati kapote\
								</div>\
								</div>\
							</div>\
						<div>\
					\
					\
					\
					";
                    return template_in_out;
                },
                "targets": 2
			},
			{ "visible": false,  "targets": [ 3 ] },    
			
		],
		"fnDrawCallback": function( oSettings ) {
			$("#m_table_1 thead").remove();
       },
    } );
} );
</script>

