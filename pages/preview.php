<style>
.m-widget6 .m-widget6__body .m-widget6__item .m-widget6__text {
    display: table-cell;
    width: 23%;
    padding-left: 0;
    padding-right: 0;
    vertical-align: top;
    font-size: 1rem;
}

.m-widget6 .m-widget6__head .m-widget6__item .m-widget6__caption {
    display: table-cell;
    width: 23%;
    padding-left: 0;
    padding-right: 0;
}
</style>


<div class="row">
    <div class="col-md-12">
        <!--begin::Portlet-->

                  
                                
                                <div class="m-portlet">
                                    <div class="m-portlet__body  m-portlet__body--no-padding">
                                        <div class="row m-row--no-padding m-row--col-separator-xl">
                                            <div class="col-xl-4">
                                                <!--begin:: Widgets/Daily Sales-->
                                                <div class="m-widget14">
                                                    <div class="m-widget14__header m--margin-bottom-30">
                                                        <h3 class="m-widget14__title">
                                                            <b>Προεπισκόπηση κρατήσεων</b>
                                                        </h3>
                                                        <span class="m-widget14__desc">
                                                         Πορεία κρατήσεων 15 ημερών
                                                        </span>
                                                    </div>
                                                    <div class="m-widget14__chart" style="height:120px;">
                                                        <canvas  id="m_chart_daily_sales_custom"></canvas>
                                                    </div>
                                                </div>
                                                <!--end:: Widgets/Daily Sales-->
                                            </div>
                                            <div class="col-xl-4">
                                                <!--begin:: Widgets/Profit Share-->
                                                <div class="m-widget14">
                                                    <div class="m-widget14__header">
                                                        <h3 class="m-widget14__title">
                                                            <b>Κρατήσεις ημέρας</b>
                                                        </h3>
                                                        <span class="m-widget14__desc">
                                                            Αναλυτικά οι κρατήσεις σήμερα
                                                        </span>
                                                    </div>
                                                    <div class="row  align-items-center">
                                                        <div class="col">
                                                            <div id="m_chart_profit_share_custom" class="m-widget14__chart" style="height: 160px">
                                                                <div class="m-widget14__stat">
                                                                    125
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget14__legends">
                                                                <div class="m-widget14__legend">
                                                                    <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                                                    <span class="m-widget14__legend-text">
                                                                        37% Parkfast
                                                                    </span>
                                                                </div>
                                                                <div class="m-widget14__legend">
                                                                    <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                                                    <span class="m-widget14__legend-text">
                                                                        47% Tηλεφωνικές
                                                                    </span>
                                                                </div>
                                                                <div class="m-widget14__legend">
                                                                    <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                                                    <span class="m-widget14__legend-text">
                                                                        19% Direct
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end:: Widgets/Profit Share-->
                                            </div>
                                            <div class="col-xl-4">
                                                <!--begin:: Widgets/Revenue Change-->
                                                <div class="m-widget14">
                                                    <div class="m-widget14__header">
                                                        <h3 class="m-widget14__title">
                                                          <b>Kινήσεις ημέρας</b>
                                                        </h3>
                                                        <span class="m-widget14__desc">
                                                            Κατάσταση κινήσεων ήμερας
                                                        </span>
                                                    </div>
                                                    <div class="row  align-items-center">
                                                        <div class="col">
                                                            <div id="m_chart_revenue_change_custom" class="m-widget14__chart1" style="height: 180px"></div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="m-widget14__legends">
                                                                <div class="m-widget14__legend">
                                                                    <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                                                    <span class="m-widget14__legend-text">
                                                                        15 Είσοδοι
                                                                    </span>
                                                                </div>
                                                                <div class="m-widget14__legend">
                                                                    <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                                                    <span class="m-widget14__legend-text">
                                                                        17 Έξοδοι
                                                                    </span>
                                                                </div>
                                                                <div class="m-widget14__legend">
                                                                    <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                                                    <span class="m-widget14__legend-text">
                                                                        50 ολοκληρώθηκαν
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end:: Widgets/Revenue Change-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    
                           
    
















                        <div class="row">
                            <div class="col-xl-6">
								<!--begin:: Widgets/Product Sales-->
								<div class="m-portlet m-portlet--bordered-semi m-portlet--space m-portlet--full-height ">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
                                                          Πωλήσεις ημέρας
													<span class="m-portlet__head-desc">
                                                       Αναλυτικά οι πωλήσεις σήμερα
													</span>
												</h3>
											</div>
										</div>
									</div>
                                    <div class="m-portlet__body">
										<div class="m-widget25">
											<span class="m-widget25__price m--font-brand">
												€150,41
											</span>
											<span class="m-widget25__desc">
                                                  <b>Σύνολο σήμερα</b>
											</span>
											<div class="m-widget25--progress">
												<div class="m-widget25__progress">
													<span class="m-widget25__progress-number">
														100€
													</span>
													<div class="m--space-10"></div>
													<div class="progress m-progress--sm">
														<div class="progress-bar m--bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
													<span class="m-widget25__progress-sub">
														Μετρητά
													</span>
												</div>
												<div class="m-widget25__progress">
													<span class="m-widget25__progress-number">
														50,41
													</span>
													<div class="m--space-10"></div>
													<div class="progress m-progress--sm">
														<div class="progress-bar m--bg-accent" role="progressbar" style="width: 39%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
													<span class="m-widget25__progress-sub">
														Κάρτες/Online
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>






                            <div class="col-xl-6">
								<!--begin:: Widgets/Sales States-->
								<div class="m-portlet m-portlet--full-height ">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text">
                                                      Kινήσεις δεκαπενθήμερου
												</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="m-widget6">
											<div class="m-widget6__head">
												<div class="m-widget6__item">
													<span class="m-widget6__caption">
                                                        Ημερ/νια
													</span>
													<span class="m-widget6__caption">
														Νέες κρατήσεις
													</span>
                                                    <span class="m-widget6__caption">
                                                       Είσοδοι
													</span>
                                                    <span class="m-widget6__caption">
                                                       Έξοδοι
													</span>
													<span class="m-widget6__caption m--align-right">
														Ποσό
													</span>
												</div>
											</div>
											<div class="m-widget6__body">
												<div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
												<div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
                                                <div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
                                                <div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
                                                <div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
                                                <div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
                                                <div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
                                                <div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
                                                <div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
                                                <div class="m-widget6__item">
													<span class="m-widget6__text">
														16/13/17
													</span>
													<span class="m-widget6__text">
														67
													</span>
                                                    <span class="m-widget6__text">
														45
													</span>
                                                    <span class="m-widget6__text">
														39
													</span>
													<span class="m-widget6__text m--align-right m--font-boldest m--font-brand">
														€14,740
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end:: Widgets/Sales States-->
							</div>
   
            
        </div>
    </div>
</div>



<script>
	$(document).ready(function() {
            $("#m_chart_revenue_change_custom").length&&Morris.Donut( {
                element:"m_chart_revenue_change_custom", data:[ {
                    label: "Είσοδοι", value: 10
                }
                , {
                    label: "Έξοδοι", value: 7
                }
                , {
                    label: "Ολοκληρώθηκαν", value: 20
                }
                ], colors:[mApp.getColor("accent"), mApp.getColor("warning"), mApp.getColor("brand")]
            }
            );



    var e = new Chartist.Pie("#m_chart_profit_share_custom", {
        series: [{
            value: 37,
            className: "custom",
            meta: {
                color: mApp.getColor("brand")
            }
        }, {
            value: 47,
            className: "custom",
            meta: {
                color: mApp.getColor("accent")
            }
        }, {
            value: 19,
            className: "custom",
            meta: {
                color: mApp.getColor("warning")
            }
        }],
        labels: [1, 2, 3]
    }, {
        donut: !0,
        donutWidth: 17,
        showLabel: !1
    });
    e.on("draw", function(e) {
        if ("slice" === e.type) {
            var t = e.element._node.getTotalLength();
            e.element.attr({
                "stroke-dasharray": t + "px " + t + "px"
            });
            var a = {
                "stroke-dashoffset": {
                    id: "anim" + e.index,
                    dur: 1e3,
                    from: -t + "px",
                    to: "0px",
                    easing: Chartist.Svg.Easing.easeOutQuint,
                    fill: "freeze",
                    stroke: e.meta.color
                }
            };
            0 !== e.index && (a["stroke-dashoffset"].begin = "anim" + (e.index - 1) + ".end"), e.element.attr({
                "stroke-dashoffset": -t + "px",
                stroke: e.meta.color
            }), e.element.animate(a, !1);
        }
    }), e.on("created", function() {
        window.__anim21278907124 && (clearTimeout(window.__anim21278907124), window.__anim21278907124 = null), window.__anim21278907124 = setTimeout(e.update.bind(e), 15e3);
    });






  });




  var e = $("#m_chart_daily_sales_custom");
                if (0 != e.length) {
                    var t = {
                        labels: ["Label 1", "Label 2", "Label 3", "Label 4", "Label 5", "Label 6", "Label 7", "Label 8", "Label 9", "Label 10", "Label 11", "Label 12", "Label 13", "Label 14", "Label 15"],
                        datasets: [{
                            backgroundColor: mApp.getColor("success"),
                            data: [15, 20, 25, 30, 25, 20, 15, 20, 25, 30, 25, 20, 15, 10, 15]
                        }]
                    };
                    new Chart(e, {
                        type: "bar",
                        data: t,
                        options: {
                            title: {
                                display: !1
                            },
                            tooltips: {
                                intersect: !1,
                                mode: "nearest",
                                xPadding: 10,
                                yPadding: 10,
                                caretPadding: 10
                            },
                            legend: {
                                display: !1
                            },
                            responsive: !0,
                            maintainAspectRatio: !1,
                            barRadius: 4,
                            scales: {
                                xAxes: [{
                                    display: !1,
                                    gridLines: !1,
                                    stacked: !0
                                }],
                                yAxes: [{
                                    display: !1,
                                    stacked: !0,
                                    gridLines: !1
                                }]
                            },
                            layout: {
                                padding: {
                                    left: 0,
                                    right: 0,
                                    top: 0,
                                    bottom: 0
                                }
                            }
                        }
                    });
                }
</script>