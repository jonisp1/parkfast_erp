<style>
.m-form .m-form__group {
margin-bottom: 0;
padding-top: 0px;
padding-bottom: 0px;
}

.m-form.m-form--fit .m-form__content, .m-form.m-form--fit .m-form__group, .m-form.m-form--fit .m-form__heading {
    padding-left: 0px;
    padding-right: 0px;
}
</style>
<div class="modal fade" id="new_reservation_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered  modal-lg" role="document" style="max-width: 1080px !important;">
								<div class="modal-content">
									<div class="modal-header" style="border-bottom: 0px solid #e9ecef; padding:0px !important;">
										<h5 class="modal-title text-nowrap" id="exampleModalLongTitle" style="padding-top: 1.3%;padding-left: 3%;">
											ΠΡΟΣΘΗΚΗ ΚΡΑΤΗΣΗΣ
										</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close" style='padding-top: 3.5%; padding-right: 3%';>
											<span aria-hidden="true">
												&times;
											</span>
										</button>
									</div>
									<form class="m-form m-form--state m-form--fit " id="new_reservation_form">
									<div class="modal-body" style="margin-top: -1%;">
									<div class="row" style='height: 100px; background-color: #ddddddb3; margin-left: -25px; margin-right: -25px;'>
											<div class="col-xl-4" style='padding-top: 1.2%; padding-left: 3%;'>
											<b style="color: #0009;">ΗΜΕΡ/ΝΙΑ ΑΦΙΞΗΣ</b>
												<div class="m-input-icon m-input-icon--left" style='padding-top: 1.2%;'>
													<input type="text" class="form-control m-input" readonly  placeholder="Επιλέξτε ημερομηνία καί ώρα" style='background-color: white;' id="datetimepicker_date_in"/>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-calendar-check-o"></i>
														</span>
													</span>   
												</div>
											</div>
											<div class="col-xl-4" style='padding-top: 1.2%; padding-left: 3%;'>
											<b style="color: #0009;">ΗΜΕΡ/ΝΙΑ ΑΝΑΧΩΡΗΣΗΣ</b>
											<div class="m-input-icon m-input-icon--left" style='padding-top: 1.2%;'>
													<input type="text" class="form-control m-input" readonly  placeholder="Επιλέξτε ημερομηνία καί ώρα" style='background-color: white;' id="datetimepicker_date_out"/>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-calendar-check-o"></i>
														</span>
													</span>   
												</div>
											</div>
											<div class="col-xs-12 col-xl-4" style="background-color: #efefef;">
												<div class="row">
													<div class="col-md-12" style="max-width: 80%; padding-left: 18%; padding-top: 3.8%;">
																<div class="form-group m-form__group row">
																	<select id="nr_reservation_type" class="form-control m-bootstrap-select m_selectpicker">
																		<option>
																			-
																		</option>
																	</select>
																</div>
													</div>
													<div class="col-md-12">
													<div class="row" style="padding-left: 11%; padding-top: 3%; color: #0009;">
														<div class="col-md-8">
														 <b style="font-size:12px">Κοστος σταθμευσης</b> <br>   <b style="font-size:11px">για <b id='nr_time_park'>10</b> <b id='nr_time_park_text'>ημέρες </b></b>
														</div>
														<div class="col-md-4" style="padding-top:2.6%">
														<b>15€</b>
														</div>
													</div>
													</div>
												</div>
											</div>
											</div>
											<div class="row"  style='height: 250px; padding-top: 2.2%;'>
											<div class="col-xl-4">
												<b class="text-nowrap" style="color: #0009;">Πελάτης</b>
												<div style='padding-top: 3.2%;' class="form-group m-form__group">
												<div class=" m-input-icon m-input-icon--left input-group">
													<input type="text" class="form-control m-input " placeholder="Ονομ/νυμο" id='nr_name' autocomplete='off' name='nr_name'>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-user"></i>
														</span>
													</span>
												</div>
												</div>
												<div style='padding-top: 3.2%;' class="form-group m-form__group">
												<div class="m-input-icon m-input-icon--left input-group">
													<input type="text" class="form-control m-input"   placeholder="Εισάγετε τηλέφωνο για αναζήτηση" id='nr_phone' autocomplete='off' name='nr_phone'>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-phone"></i>
														</span>
													</span>
												</div>
												</div>
												<div style='padding-top: 3.2%;' class="form-group m-form__group">
												<div class="m-input-icon m-input-icon--left input-group">
													<input type="text" class="form-control m-input"   placeholder="Email" id="nr_email" autocomplete='off' name="nr_email">
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-at"></i>
														</span>
													</span>
												</div>
											</div>
											</div>
											<div class="col-xl-4">
											<div class='row'>
												<b style="padding-left: 5.5%; color: #0009;" class="text-nowrap">Λοιπά στοιχεία</b>
												<div class="col-xl-12" style='padding-top: 3.2%;' class="form-group m-form__group">
												<div class="m-input-icon m-input-icon--left">
													<input type="text" class="form-control m-input input-group"   placeholder="Αεροπορική Εταιρεία" id="nr_plane_company" name="nr_plane_company">
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-plane"></i>
														</span>
													</span>
												</div>
												</div>
												<div class="col-xl-6" style='padding-top: 3.2%;' class="form-group m-form__group">
												<div class="m-input-icon m-input-icon--left">
													<input type="text" class="form-control m-input input-group"   placeholder="Πόλη Επισ." id="nr_visit_city"  name="nr_visit_city">
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-building"></i>
														</span>
													</span>
												</div>
												</div>
												<div class="col-xl-6" style='padding-top: 3.2%;' class="form-group m-form__group">
												<div class="m-input-icon m-input-icon--left">
													<input type="text" class="form-control m-input"   placeholder="Αρ.Ατόμων" id="nr_people_count" autocomplete='off' name="nr_people_count">
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-users"></i>
														</span>
													</span>
												</div>
											</div>
											<div class="col-xl-12" style='padding-top: 3.2%;' class="form-group m-form__group">
												<div class="m-input-icon m-input-icon--left">
													<input type="text" class="form-control m-input"   placeholder="Θέση" id="nr_spot" name="nr_spot" autocomplete='off'>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-underline"></i>
														</span>
													</span>
												</div>
												</div>
											</div>
											</div>
											<div class="col-xs-12 col-xl-4">
											
											<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false" style="margin-right: -20px; margin-left: -13px; margin-top: -7%; background: #efefef;">
												<div style="padding-top: 7.1%; padding-left:2%;" >
												<b style="color: #0009;">Επιπλέον Υπηρεσίες</b>
												</div>
													<div class="m-demo__preview" style="margin-top: 1%; padding-bottom: 0px !important;">
														<form class="m-form">
															<div class="m-form__group form-group row" style="height:100px; overflow-y: scroll;">
																<div class="col-9">
																	<div class="m-checkbox-list" id="nr_services_div">
																		
																		
																	</div>
																</div>
															</div>
															</form>
															</div>
															</div>
											</div>
											</div>
											<div class="row">
											<div class="col-xs-12 col-xl-4" style="margin-top:-5%">
											<div class='row'>
											<div style="padding-left: 4.2%;" class="form-group m-form__group text-nowrap"> 
											<b style="color: #0009; ">Στοιχεία οχήματος</b>
											</div>
												<div class="col-md-12" style='padding-top: 3.2%;'>
												<div class="m-input-icon m-input-icon--left">
													<input type="text" class="form-control m-input" list="nr_plates"  placeholder="Πινακίδα" id="nr_plate" name="nr_plate" autocomplete='off'>
													<datalist id="nr_plates">
											    	</datalist>
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-car"></i>
														</span>
													</span>
												</div>
												</div>
												<div class="col-md-6" style='padding-top: 3.2%;'>
												<div class="m-input-icon m-input-icon--left">
													<input type="text" class="form-control m-input"   placeholder="Μάρκα" id="nr_brand" name="nr_brand" >
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-tag"></i>
														</span>
													</span>
												</div>
												</div>
												<div class="col-md-6" style='padding-top: 3.2%;'>
												<div class='form-group m-form__group'>
												<div class="m-input-icon m-input-icon--left">
													<input type="text" class="form-control m-input"   placeholder="Χρώμα" id="nr_color" name="nr_color">
													<span class="m-input-icon__icon m-input-icon__icon--left">
														<span>
															<i class="la la-paint-brush"></i>
														</span>
													</span>
													</div>
												</div>
												</div>
												</div>
											</div>
											<div class="col-xl-4" style="margin-top:-5%">
											<b style="color: #0009; flex-wrap: nowrap;">Σχόλια</b>
												<div style='padding-top: 3.2%;'>
													<textarea class="form-control m-input" name="memo" placeholder="Γράψε εδώ..." rows="3" id="nr_comments" autocomplete='off' name="nr_comments"></textarea>
												</div>
											</div>
											<div class="col-xl-4">
																<div style="padding-left:18%; margin-top: -40px;" class="form-group m-form__group">
																	<div class="m-radio-inline">
																		<label class="m-radio">
																			<input type="radio" name="nr_reciept_type" value="1">
																			ΑΠΟΔΕΙΞΗ
																			<span></span>
																		</label>
																		<label class="m-radio">
																			<input type="radio" name="nr_reciept_type" value="2">
																			ΤΙΜΟΛΟΓΙΟ
																			<span></span>
																		</label>
																	</div>
																</div>
																<div style="padding-left:33%; padding-top:1%;" >
																	<label class="m-checkbox m-checkbox--bold m-checkbox--state-brand">
																		<input type="checkbox" name='nr_prepay' value='1'>
																		Προπληρωμή
																		<span></span>
																	</label>
																</div>
																<div>
																	<button type="submit" id="nr_submit_button" class="btn m-btn--square  btn-success btn-block" >
																		ΕΝΑΡΞΗ ΧΡΕΩΣΗΣ
																	</button>
																</div>
											</div>
									</div>
									</div>
								</div>
								</div>
							</div>
</div>