
						<div class="row">
							<div class="col-md-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon m--hide">
													<i class="la la-gear"></i>
												</span>
												<h3 class="m-portlet__head-text">
													ΦΙΛΤΡΑ
												</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body">
                                      
                                        <div class="form-group m-form__group row">
												<div class="col-lg-4">
													<label>
														ΑΝΑΖΗΤΗΣΗ
													</label>
													<div class="m-input-icon m-input-icon--left">
														<input type="text" class="form-control m-input" placeholder="Phone number">
														<span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                                    </div>
												</div>
												<div class="col-lg-3">
                                                    <label>
                                                       ΗΜΕΡ/ΝΙΑ ΑΦΙΞΗΣ
                                                    </label>
                                                   
                                                        <div class="m-input-icon m-input-icon--left">
                                                            <input type="text" class="form-control m-input" readonly  placeholder="Select date" id="m_datepicker_2"/>
                                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                                <span>
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </span>
                                        
                                                    </div>
												</div>
												<div class="col-lg-3">
                                                    <label>
                                                       ΗΜΕΡ/ΝΙΑ ΑΝΑΧΩΡΗΣΗΣ
                                                    </label>
                                                   
                                                        <div class="m-input-icon m-input-icon--left">
                                                            <input type="text" class="form-control m-input" readonly  placeholder="Select date" id="m_datepicker_2"/>
                                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                                <span>
                                                                    <i class="la la-calendar-check-o"></i>
                                                                </span>
                                                            </span>
                                        
														</div>
												</div>
												<div class="col-lg-2" style="padding-top:2%">
													<a href="#" class="btn btn-primary m-btn m-btn--sm "  style="font-size: 13px">
													<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Αναζήτηση&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>					
													</a>
												</div>
											</div>



                                    </div>
                                </div>
							</div>
						</div>


								

								<div class="row">
										<div class="col-md-12">
											<!--begin::Portlet-->
											<div class="m-portlet m-portlet--tabs  m-portlet--head-solid-bg m-portlet--bordered">
													<div class="m-portlet__head">
													
												<div class="m-portlet__body">
														<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1" style='font-family: Roboto Slab !important;'>
																<thead>
                                                                <tr style="font-size: smaller; color: #909090;">
                                                                    <th colspan="1" style="padding-left: 8%; border-bottom: 0px solid #9f9f9f !important;">
                                                                        ΣΤΟΙΧΕΙΑ ΣΥΜΒΟΛΑΙΟΥ
                                                                    </th>
                                                                    <th colspan="1" style="border-bottom: 0px solid #9f9f9f !important;">
                                                                        <span style="padding-left: 10%;"> ΕΝΑΡΞΗ</span>  <span  style="padding-left: 18%;">ΟΛΟΚΛΗΡΩΣΗ</span>
                                                                    </th>
                                                                    <th colspan="1" style="border-bottom: 0px solid #9f9f9f !important;">
                                                                        <span style="padding-left: 4%;"> ΟΧΗΜΑΤΑ</span>  <span  style="padding-left: 23%;">ΣΥΝΟΛΙΚΟ ΚOΣΤΟΣ</span>
                                                                    </th>
                                                                </tr>
																	<tr id='this_head'>
																		<th>
																			RecordID
																		</th>
																		<th>
																			CompanyEmail
																		</th>
																		<th>
																			CompanyAgent
																		</th>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>
																			1
																		</td>
																		<td >
																			nsailor0@livejournal.com
																		</td>
																		<td>
																			Nixie Sailor
																		</td>
																		<td nowrap></td>
																	</tr>
																	<tr>
																		<td>
																			2
																		</td>
																		<td>
																			egiraldez1@seattletimes.com
																		</td>
																		<td>
																			Emelita Giraldez
																		</td>
																		<td nowrap></td>
																	</tr>
																	<tr>
																		<td>
																			3
																		</td>
																		<td>
																			uluckin2@state.gov
																		</td>
																		<td>
																			Ula Luckin
																		</td>
																		<td nowrap></td>
																	</tr>
																</tbody>
															</table>
			
			
			
												</div>
											</div>
										</div>
									<div>


<script>
	$(document).ready(function() {
    $('#m_table_1').DataTable( {
		"columnDefs": 
		[
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {

					var template_in_out = 
                    "\
                    <div class='row' style='padding-top: 1%;'>\
						<div class='col-md-4'>\
						</div>\
						<div class='col-md-8' style='padding-left: 22.5px; font-size: 26px;'>\
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
						</div>\
					</div>\
					<div class='row'>\
                    <div class='col-md-4'>\
                        \
                    </div>\
                    <div class='col-md-8'>\
                            <a  class='btn btn-outline-metal m-btn m-btn--icon inactiveLink'>\
                                <span>\
                                    <i class='la la-car' style='color: #0000ffb3;'></i>\
                                    <span>\
                                        <b>EEE1332</b>\
                                    </span>\
                                </span>\
                            </a>\
                            <div class='row' style='padding-left: 7%; padding-top: 1.5%;'>\
                                <div style='font-size:12px;'>BMW Μάυρο</div>\
                            </div>\
                    </div>\
                    </div>\
                    <div class='row'>\
                    <div class='col-md-4'>\
                             <div class='row' style='padding-left: 15%; padding-top: 9%;'>\
                                <b style='font-size:17px;'>ASERTG32</b>\
                            </div>\
                    </div>\
                    <div class='col-md-8' style='padding-top: 2%;'>\
                      <hr>\
                        <div class='row' style='font-size:13px; padding-left: 5.8%;'>\
                        <a class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill inactiveLink' style='width: 20px;height: 20px; background-color: #fff;'><i class='la la-user'></i></a>\
                        <div style='padding-left: 3%;'>Καραμίτσος αποστόλος</div>\
                        </div>\
                        <div class='row' style='font-size:13px; padding-left: 5.8%; padding-top: 2%;'>\
                        <a class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill inactiveLink' style='width: 20px;height: 20px; background-color: #fff;'><i class='la la-phone'></i></a>\
                        <div style='padding-left: 3%;'>5432342122</div>\
                        </div>\
                        <div class='row' style='font-size:13px; padding-left: 5.8%; padding-top: 2%;'>\
                        <a class='btn btn-metal m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill inactiveLink' style='width: 20px;height: 20px; background-color: #fff;'><i class='la la-at'></i></a>\
                        <div style='padding-left: 3%;'>karakapoios@gmail.con</div>\
                        </div>\
                    </div>\
                    </div>\
                    <div class='row' style='padding-top: 1%;'>\
						<div class='col-md-4'>\
						</div>\
						<div class='col-md-8' style='padding-left: 22.5px; font-size: 26px;'>\
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\
						</div>\
					</div>\
					";
                    return template_in_out;
                },
                "targets": 0
            },
			{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {

					var template_in_out = 
					"\
					<div class='row'>\
						<div class='col-md-5 center-block text-center' style='center-block text-center'>\
							<div class='row' style='font-size: 14px; color: #929292; padding-left: 45%; padding-top: 18%;' >\
								<b>22/12/2016</b>\
							</div>\
							<div class='row'  style='padding-top: 3%; font-size: 15px; padding-left: 45%'>\
								<b>11:45</b>\
							</div>\
							<div class='row'  style='padding-top: 3%; font-size: 14px; color: #929292; padding-left: 100%; margin-top: 40%;'>\
                              <a  class='btn btn-outline-metal m-btn m-btn--icon inactiveLink'>\
                                <span>\
                                    <i class='la la-refresh' style='color: #00d120;'></i>\
                                    <span>\
                                        <b>Ανανέοση</b>\
                                    </span>\
                                </span>\
                              </a>\
							</div>\
						</div>\
						<div class='col-md-2 center-block text-center' style='center-block text-center; padding-left: 7%;'>\
							<div style='border-left:2px solid #0000004d;height:85px'></div>\
						</div>\
						<div class='col-md-5 center-block text-center' style='center-block text-center' style='padding-left: 0%;'>\
							<div class='row' style='font-size: 14px; color: #929292; padding-top: 18%;' >\
								<b>22/12/2016</b>\
							</div>\
							<div class='row'  style='padding-top: 3%; font-size: 15px;'>\
								<b>11:45</b>\
							</div>\
                          </div>\
						</div>\
					\
					\
					\
					";
                    return template_in_out;
                },
                "targets": 1
            },
			{
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {

					var template_in_out = 
					"\
						<div class='row'>\
							<div class='col-md-3'>\
								<div class='row'  style='padding-left: 25%'>\
                                <div style='padding-top:4%;'>\
									<a  class='btn btn-outline-metal m-btn m-btn--icon inactiveLink'>\
                                        <span>\
                                            <i class='la la-car' style='color: #0000ffb3;'></i>\
                                            <span>\
                                                <b>EAY1302</b>\
                                            </span>\
                                        </span>\
                                    </a>\
                                    </div>\
                                    <div style='padding-top:4%;'>\
									<a  class='btn btn-outline-metal m-btn m-btn--icon inactiveLink'>\
                                        <span>\
                                            <i class='la la-car' style='color: #0000ffb3;'></i>\
                                            <span>\
                                                <b>EAY1302</b>\
                                            </span>\
                                        </span>\
                                    </a>\
                                    </div>\
								</div>\
							</div>\
							<div class='col-md-1'>\
							</div>\
							<div class='col-md-8' style='padding-left: 12%;'>\
								<div class='row'>\
								<div style='color:#56b300; font-size: x-large;'> 14,40€ </div>\
								</div>\
								<div class='row' style='word-wrap: break-word; '>\
								<div class='col-md-12' style='padding-left: 0px !important; font-size:13px;'>\
								<hr>\
								<b style='color: #929292;'>Σχόλια :</b>\
								kati kapote\
								</div>\
								</div>\
							</div>\
						<div>\
					\
					\
					\
					";
                    return template_in_out;
                },
                "targets": 2
			},
			{ "visible": false,  "targets": [ 3 ] },    
			
		],
		"fnDrawCallback": function( oSettings ) {
			$("#this_head").remove();
       },
    } );
} );
</script>

