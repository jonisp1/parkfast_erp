<?php

class OrderRequest {}
class PaymentRequest {}
	class CancelRequest {}

class NativeCheckout {

	//Demo
		// private $merchantId = 'faf27b08-55bf-454b-ab97-761f8e43abde';
		// private $apiKey = '+@0#5V'; 
		// private $baseApiUrl = "https://demo.vivapayments.com";
		// private $nativeCheckoutSourceCode = "1163";
	//** Demo

	//production
		private $merchantId = 'e2803cd8-56b1-48cb-8e2d-2389ca48dc83';
		private $apiKey = 'park'; 
	 	private $baseApiUrl = "https://www.vivapayments.com";
	    private $nativeCheckoutSourceCode = "5436";
	//** production


    private $paymentsUrl = "/api/transactions";
    private $paymentsCreateOrderUrl = "/api/orders";
    // A Source for which its Integration method is 
    // set to "Native/Pay with Viva Wallet" option.
    // See http://demo.vivapayments.com/selfcare/en-US/sources/paymentsources
	
	private $resultObj = "";
	
	public function MakePayment($amount,$cardToken,$installments){
		
		$orderCode=$this->CreateOrder($amount,$installments);
		
		$obj=new PaymentRequest();
		
		$obj->Amount=$amount;
		$obj->OrderCode=$orderCode;
		$obj->SourceCode=$this->nativeCheckoutSourceCode;
		$obj->CreditCard["Token"]=$cardToken;
		$obj->Installments=$installments;
		
		$resultObj = $this->ExecuteCall($this->baseApiUrl.$this->paymentsUrl,$obj);
				
		if ($resultObj->ErrorCode==0){	//success when ErrorCode = 0
			return $resultObj->TransactionId;
		}
		else{
			echo 'The following error occured: ' . $resultObj->ErrorText;
			return '0';
		}	
	}

	public function CancelPayment($amount,$payment_id){
		
		//$orderCode=$this->CreateOrder($amount,$installments);
		
		$obj=new CancelRequest();
		
		//$obj->amount=urlencode($amount);
		 
		$pay_url = '/api/transactions/'.$payment_id.'?amount='.urlencode($amount);

		$resultObj = $this->ExecuteCall_2($this->baseApiUrl.$pay_url,$obj);
				
		return $resultObj->StatusId;
		
	}
	
	private function CreateOrder($amount,$installments){
	
		$obj=new OrderRequest();
		
		$obj->Amount=$amount;
		$obj->SourceCode=$this->nativeCheckoutSourceCode;
		$obj->MaxInstallments=$installments;
				
		$resultObj = $this->ExecuteCall($this->baseApiUrl.$this->paymentsCreateOrderUrl,$obj);
		
		if ($resultObj->ErrorCode==0){	//success when ErrorCode = 0
			return $resultObj->OrderCode;
		}
		else{
			echo 'The following error occured: ' . $resultObj->ErrorText;
			return 0;
		}	
	}	
	
	private function ExecuteCall($postUrl,$postobject){
	
		$postargs=json_encode($postobject);
	
		// Get the curl session object
		$session = curl_init($postUrl);
		
		// Set the POST options.
		curl_setopt($session, CURLOPT_POST, true);
		curl_setopt($session, CURLOPT_POSTFIELDS, $postargs);
		curl_setopt($session, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($postargs))                                                                       
		);   
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($session, CURLOPT_USERPWD, $this->merchantId.':'.$this->apiKey);
		curl_setopt($session, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		
		curl_setopt($session, CURLOPT_HEADER, true);
		
		// Do the POST and then close the session
		$response = curl_exec($session);
		
		// Separate Header from Body
		$header_len = curl_getinfo($session, CURLINFO_HEADER_SIZE);
		$resHeader = substr($response, 0, $header_len);
		$resBody =  substr($response, $header_len);
		
		// Parse the JSON response
		try {
			if(is_object(json_decode($resBody))){
				$resultObj=json_decode($resBody);
			}else{
				preg_match('#^HTTP/1.(?:0|1) [\d]{3} (.*)$#m', $resHeader, $match);
				throw new Exception(trim($match[1]));
			}
		} catch( Exception $e ) {
			echo $e->getMessage();
		}
		
		curl_close($session);
		return $resultObj;
	}

	private function ExecuteCall_2($postUrl,$postobject){
	
		$postargs=json_encode($postobject);
	
		// Get the curl session object
		$session = curl_init($postUrl);

		
		// Set the POST options.
		curl_setopt($session, CURLOPT_URL, $postUrl);

 	    curl_setopt($session, CURLOPT_CUSTOMREQUEST, 'DELETE');
		 curl_setopt($session, CURLOPT_POSTFIELDS, $postargs);
		curl_setopt($session, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($postargs))                                                                       
		);   
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($session, CURLOPT_USERPWD, $this->merchantId.':'.$this->apiKey);
		curl_setopt($session, CURLOPT_SSL_CIPHER_LIST, 'TLSv1');
		
		curl_setopt($session, CURLOPT_HEADER, true);
		
		// Do the POST and then close the session
		$response = curl_exec($session);
		
		// Separate Header from Body
		$header_len = curl_getinfo($session, CURLINFO_HEADER_SIZE);
		$resHeader = substr($response, 0, $header_len);
		$resBody =  substr($response, $header_len);
		
		// Parse the JSON response
		try {
			if(is_object(json_decode($resBody))){
				$resultObj=json_decode($resBody);
			}else{
				preg_match('#^HTTP/1.(?:0|1) [\d]{3} (.*)$#m', $resHeader, $match);
				throw new Exception(trim($match[1]));
			}
		} catch( Exception $e ) {
			echo $e->getMessage();
		}
		
		curl_close($session);
		return $resultObj;
	}
	
	
}
?>
