<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<!-- BEGIN: Left Aside -->
				<button class="m-aside-left-close m-aside-left-close--skin-light" id="m_aside_left_close_btn">
					<i class="la la-close"></i>
				</button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-light ">


					<!-- BEGIN: Aside Menu -->
					<!-- CURRENT MENU ID COUNTER = 14 -->
	<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-light m-aside-menu--submenu-skin-light m-aside-menu--dropdown " data-menu-vertical="true" m-menu-dropdown="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" >
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow " style="height: 90px;">
						<li class="m-menu__item m-menu__item--active" aria-haspopup="true" id='menu_item_0' onClick='menu_state_change("menu_item_0"); load_page("preview.php" , "Προεπισκόπηση");' >
								<a  class="m-menu__link ">
									<span class="m-menu__item-here"></span>
									<i class="m-menu__link-icon flaticon-imac"></i>
									<span class="m-menu__link-text">
									Προεπισκόπηση
									</span>
								</a>
							</li> 
							<li class="m-menu__item" aria-haspopup="true"  id='menu_item_1' onClick='menu_state_change("menu_item_1"); load_page("in_out.php" , "Είσοδοι/Έξοδοι");'>
								<a class="m-menu__link ">
									<span class="m-menu__item-here"></span>
									<i class="m-menu__link-icon flaticon-more-v4" onClick=""></i>
									<span class="m-menu__link-text">
										Είσοδοι/Έξοδοι
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover"  id='menu_item_2' onClick='menu_state_change("menu_item_2");'>
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<span class="m-menu__item-here"></span>
									<i class="m-menu__link-icon flaticon-notes"></i>
									<span class="m-menu__link-text">
										Κρατήσεις
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu " id='menu_item_3' onClick='menu_state_change("menu_item_3");'>
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link"  >
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  id='menu_item_4' onClick='menu_state_change("menu_item_4");   load_page("complete_reservations.php" , "Ολοκληρωμένες κρατήσεις");'>
											<a  class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot" >
													<span></span>
												</i>
												<span class="m-menu__link-text">
														Ολοκληρωμένες  Κρατήσεις
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1"  id='menu_item_5' onClick='menu_state_change("menu_item_5");  load_page("canceled_reservations.php" , "Ακυρωμένες Κρατήσεις");'>
											<a   class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Ακυρωμένες Κρατήσεις
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover"  id='menu_item_6' onClick='menu_state_change("menu_item_6");'>
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<span class="m-menu__item-here"></span>
									<i class="m-menu__link-icon flaticon-share"></i>
									<span class="m-menu__link-title">
										<span class="m-menu__link-wrap">
											<span class="m-menu__link-text">
												Συμβόλαια Στάθμευσης
											</span>
											<span class="m-menu__link-badge">

											</span>
										</span>
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
								<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true" >
											<span class="m-menu__link"  >
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  id='menu_item_14' onClick='menu_state_change("menu_item_14");   load_page("contracts_in_out.php" , "Τρέχον Συμβόλαια");'>
											<a  class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot" >
													<span></span>
												</i>
												<span class="m-menu__link-text">
														Τρέχον Συμβόλαια
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  id='menu_item_7' onClick='menu_state_change("menu_item_7");   load_page("complete_contracts.php" , "Ολοκληρωμένα Συμβόλαια");'>
											<a  class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot" >
													<span></span>
												</i>
												<span class="m-menu__link-text">
														Ολοκληρωμένα Συμβόλαια
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1"  id='menu_item_8' onClick='menu_state_change("menu_item_8");  load_page("canceled_contracts.php" , "Ακυρωμένα Συμβόλαια");'>
											<a   class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Ακυρωμένα Συμβόλαια
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
								<a  href="javascript:;" class="m-menu__link m-menu__toggle" id='menu_item_10' onClick='menu_state_change("menu_item_10");'>
									<span class="m-menu__item-here"></span>
									<i class="m-menu__link-icon flaticon-coins"></i>
									<span class="m-menu__link-text">
										Συναλλαγές
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"  m-menu-link-redirect="1">
											<span class="m-menu__link">
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">
													Support
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Reports
												</span>
											</a>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
											<a  href="javascript:;" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Cases
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
														<a  href="inner.html" class="m-menu__link ">
															<i class="m-menu__link-icon flaticon-computer"></i>
															<span class="m-menu__link-title">
																<span class="m-menu__link-wrap">
																	<span class="m-menu__link-text">
																		Pending
																	</span>
																	<span class="m-menu__link-badge">
																		<span class="m-badge m-badge--warning m-badge--wide">
																			10
																		</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
														<a  href="inner.html" class="m-menu__link ">
															<i class="m-menu__link-icon flaticon-signs-2"></i>
															<span class="m-menu__link-title">
																<span class="m-menu__link-wrap">
																	<span class="m-menu__link-text">
																		Urgent
																	</span>
																	<span class="m-menu__link-badge">
																		<span class="m-badge m-badge--danger m-badge--wide">
																			6
																		</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
														<a  href="inner.html" class="m-menu__link ">
															<i class="m-menu__link-icon flaticon-clipboard"></i>
															<span class="m-menu__link-title">
																<span class="m-menu__link-wrap">
																	<span class="m-menu__link-text">
																		Done
																	</span>
																	<span class="m-menu__link-badge">
																		<span class="m-badge m-badge--success m-badge--wide">
																			2
																		</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
														<a  href="inner.html" class="m-menu__link ">
															<i class="m-menu__link-icon flaticon-multimedia-2"></i>
															<span class="m-menu__link-title">
																<span class="m-menu__link-wrap">
																	<span class="m-menu__link-text">
																		Archive
																	</span>
																	<span class="m-menu__link-badge">
																		<span class="m-badge m-badge--info m-badge--wide">
																			245
																		</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Clients
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--dot">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Audit
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item" aria-haspopup="true" >
								<a  class="m-menu__link " id='menu_item_11' onClick='menu_state_change("menu_item_11");  load_page("reviews.php" , "Αξιολογήσεις");'>
									<span class="m-menu__item-here"></span>
									<i class="m-menu__link-icon flaticon-network"></i>
									<span class="m-menu__link-text">
									Αξιολογήσεις
									</span>
								</a>
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1"  id='menu_item_12' onClick='menu_state_change("menu_item_12");'>
								<a  href="javascript:;" class="m-menu__link m-menu__toggle">
									<span class="m-menu__item-here"></span>
									<i class="m-menu__link-icon flaticon-analytics"></i>
									<span class="m-menu__link-text">
										Αναφορές
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu ">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent" aria-haspopup="true"  m-menu-link-redirect="1">
											<span class="m-menu__link">
												<span class="m-menu__item-here"></span>
												<span class="m-menu__link-text">
													Conversions
												</span>
											</span>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--line">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Goals
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--line">
													<span></span>
												</i>
												<span class="m-menu__link-title">
													<span class="m-menu__link-wrap">
														<span class="m-menu__link-text">
															Ecommerce
														</span>
														<span class="m-menu__link-badge">
															<span class="m-badge m-badge--warning m-badge--wide m-badge--rounded">
																new
															</span>
														</span>
													</span>
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--line">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Transactions
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--line">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Funnels
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							<li class="m-menu__item  m-menu__item--submenu m-menu__item--bottom-2" aria-haspopup="true"  m-menu-submenu-toggle="hover" id='menu_item_13' onClick='menu_state_change("menu_item_13");'>
								<a  href="javascript:;" class="m-menu__link m-menu__toggle" >
									<i class="m-menu__link-icon flaticon-settings"></i>
									<span class="m-menu__link-text">
										Ρυθμίσεις
									</span>
									<i class="m-menu__ver-arrow la la-angle-right"></i>
								</a>
								<div class="m-menu__submenu m-menu__submenu--up">
									<span class="m-menu__arrow"></span>
									<ul class="m-menu__subnav">
										<li class="m-menu__item  m-menu__item--parent m-menu__item--bottom-2" aria-haspopup="true" >
											<span class="m-menu__link">
												<span class="m-menu__link-text">
													Settings
												</span>
											</span>
										</li>
										<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"  m-menu-submenu-toggle="hover" m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link m-menu__toggle">
												<i class="m-menu__link-bullet m-menu__link-bullet--line">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Profile
												</span>
												<i class="m-menu__ver-arrow la la-angle-right"></i>
											</a>
											<div class="m-menu__submenu ">
												<span class="m-menu__arrow"></span>
												<ul class="m-menu__subnav">
													<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
														<a  href="inner.html" class="m-menu__link ">
															<i class="m-menu__link-icon flaticon-computer"></i>
															<span class="m-menu__link-title">
																<span class="m-menu__link-wrap">
																	<span class="m-menu__link-text">
																		Pending
																	</span>
																	<span class="m-menu__link-badge">
																		<span class="m-badge m-badge--warning">
																			10
																		</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
														<a  href="inner.html" class="m-menu__link ">
															<i class="m-menu__link-icon flaticon-signs-2"></i>
															<span class="m-menu__link-title">
																<span class="m-menu__link-wrap">
																	<span class="m-menu__link-text">
																		Urgent
																	</span>
																	<span class="m-menu__link-badge">
																		<span class="m-badge m-badge--danger">
																			6
																		</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
														<a  href="inner.html" class="m-menu__link ">
															<i class="m-menu__link-icon flaticon-clipboard"></i>
															<span class="m-menu__link-title">
																<span class="m-menu__link-wrap">
																	<span class="m-menu__link-text">
																		Done
																	</span>
																	<span class="m-menu__link-badge">
																		<span class="m-badge m-badge--success">
																			2
																		</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
													<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
														<a  href="inner.html" class="m-menu__link ">
															<i class="m-menu__link-icon flaticon-multimedia-2"></i>
															<span class="m-menu__link-title">
																<span class="m-menu__link-wrap">
																	<span class="m-menu__link-text">
																		Archive
																	</span>
																	<span class="m-menu__link-badge">
																		<span class="m-badge m-badge--info m-badge--wide">
																			245
																		</span>
																	</span>
																</span>
															</span>
														</a>
													</li>
												</ul>
											</div>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--line">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Accounts
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--line">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Help
												</span>
											</a>
										</li>
										<li class="m-menu__item " aria-haspopup="true"  m-menu-link-redirect="1">
											<a  href="inner.html" class="m-menu__link ">
												<i class="m-menu__link-bullet m-menu__link-bullet--line">
													<span></span>
												</i>
												<span class="m-menu__link-text">
													Notifications
												</span>
											</a>
										</li>
									</ul>
								</div>
							</li>
							
						</ul>
					</div>