var customer_plates;
var reservation_or_direct;

var check_in_g;
var check_out_g;

$('#datetimepicker_date_out').datetimepicker({
    
	todayHighlight: true,
	autoclose: true,
    format: 'dd/mm/yyyy hh:ii',
    startDate: new Date(),
});

$('#datetimepicker_date_in').datetimepicker({
	todayHighlight: true,
	autoclose: true,
	format: 'dd/mm/yyyy hh:ii'
}).on('changeDate', function (selected) {
	var minDate = new Date(selected.date.valueOf());
    $('#datetimepicker_date_out').datetimepicker('setStartDate', minDate);
    
    //change button and reservation state
    document.getElementById('nr_submit_button').innerHTML = 'ΠΡΟΣΘΗΚΗ ΚΡΑΤΗΣΗΣ';
    reservation_or_direct = 0;


    
    
});

$('#datetimepicker_date_in').on('change.dp', function(e){ 
    
	var date_in = e.target.value;
	check_in_g = change_datepicker_for_db(date_in);
    //datechange_refresh(); 
    calc_price();
});

$('#datetimepicker_date_out').on('change.dp', function(e){ 

	var date_in = e.target.value;
	check_out_g = change_datepicker_for_db(date_in);
    //datechange_refresh();
    calc_price();
    
});

function calc_price()
{
    //alert(check_in_g);
    //alert(check_out_g);
    if((check_in_g ) && (check_out_g))
    {

        var checkboxes = document.getElementsByName('nr_services');
        var selected = [];
        for (var i=0; i<checkboxes.length; i++) {
            if (checkboxes[i].checked) {
                selected.push(checkboxes[i].value);
            }
        }
    
        console.log(selected);


        $.ajax('galaria/function_controller.php', { type: "post",
        data: {
            function: 'get_parking_time',
            field_1: check_in_g,
            field_2: check_out_g,
            field_3: selected
        }
        })
        .then(
            function success(result) {
                console.log(result);
                
                if(isJSON(result) != 0)
                {
                    //console.log(result);
                    var data = JSON.parse(result);
                    if((data.length > 0) && (data.length < 2))
                    {
                        
                    }
                    else
                    {
                        console.warn("search OK found more tha one user with this phone");
                        //modal_loader_hide("new_reservation_modal");
                        //customer_plates = '';
                    }
                }
                else
                {
                        console.error('search OK Not found \n response : '+result);
                        modal_loader_hide("new_reservation_modal");
                }
            },

            function fail(data) {
                console.error('Request "' + init_new_reservation.name +'" failed. \nReturned status of "' + data.statusText + '" ' + data.status +'\nreturn : ' + data.responseText);
                //modal_loader_hide("new_reservation_modal");
            }
        );
    }
}


$(document).on("click", "#new_reservation_button", function () //initiate now reservation
{
    document.getElementById("new_reservation_form").reset();


    var now_date = new Date();
    now_date = convert_datepicker_to_db_date(now_date);
    $("#datetimepicker_date_in").val(change_datetime(now_date));
    check_in_g = now_date;
    

    FormControls.init();
	init_new_reservation();
});

function new_reservation_reset_vars()
{
     customer_plates = 0;
     reservation_or_direct = 1;
     document.getElementById('nr_submit_button').innerHTML = 'ΕΝΑΡΞΗ ΧΡΕΩΣΗΣ';

     check_in_g = '';
     check_out_g = '';

}

$(document).on("focusout", "#nr_plate", function () //plate field focus out , fill from plate
{
    modal_loader_show("new_reservation_modal");
    var plate = document.getElementById('nr_plate').value;

    for(var i=0; i<customer_plates.length; i++)
    {
        if(plate == customer_plates[i]['plate'])
        {
            document.getElementById('nr_brand').value = customer_plates[i]['model'];
            document.getElementById('nr_color').value = customer_plates[i]['color'];
        }
    }
    modal_loader_hide("new_reservation_modal");
});


$(document).on("focusout", "#nr_phone", function () //phone field focus out , phone search
{
    modal_loader_show("new_reservation_modal");

    var phone = document.getElementById('nr_phone').value;

    if(phone.length > 9)
    {
         $.ajax('galaria/function_controller.php', { type: "post",
                data: {
                    function: 'phone_search',
                    field_1: phone
                }
                })
                .then(
                    function success(result) {
                        //console.log(result);
                        
                        if(isJSON(result) != 0)
                        {
                            //console.log(result);
                            var data = JSON.parse(result);
                            if((data.length > 0) && (data.length < 2))
                            {
                                document.getElementById('nr_phone').value = data[0]['phone'];
                                document.getElementById('nr_name').value = data[0]['name']+' '+data[0]['lastname'];
                                document.getElementById('nr_email').value = data[0]['email'];
                                
                                var options = '';
                                $plates = data[0]['plates'];
                                //console.log($plates);
                                for(var i=0; i<$plates.length; i++)
                                {
                                    options += '<option value="'+$plates[i]['plate']+'" />';
                                }  
                                customer_plates = $plates;
                                document.getElementById('nr_plates').innerHTML = options;

                                modal_loader_hide("new_reservation_modal");
                            }
                            else
                            {
                                console.warn("search OK found more tha one user with this phone");
                                modal_loader_hide("new_reservation_modal");
                                customer_plates = '';
                            }
                        }
                        else
                        {
                             console.error('search OK Not found \n response : '+result);
                             modal_loader_hide("new_reservation_modal");
                        }
                    },
            
                    function fail(data) {
                        console.error('Request "' + init_new_reservation.name +'" failed. \nReturned status of "' + data.statusText + '" ' + data.status +'\nreturn : ' + data.responseText);
                        modal_loader_hide("new_reservation_modal");
                    }
                );
         }
});



$(document).on("click", "#nr_complete_button", function ()  //reservation complite button
{
    //init_new_reservation();
    


   
});

var FormControls= {    //form validation 
    init:function() {
        $("#new_reservation_form").validate( {
        rules: {
            nr_name: {
                required: !0
            }
            , nr_reciept_type: {
                required: !0
            }

        }
        , invalidHandler:function(e, r) {
            var i=$("#m_form_1_msg");
            i.removeClass("m--hide").show(), mApp.scrollTo(i, -200)
           // mApp.scrollTo("#m_form_2"), swal( {
           //     title: "", text: "There are some errors in your submission. Please correct them.", type: "error", confirmButtonClass: "btn btn-secondary m-btn m-btn--wide"
           // })
        }
        , submitHandler:function(e) {

            alert('eeeooo');
        }
        }
        );
    }
}

function init_new_reservation() //fill field on new reservation modals
{
    page_load_show();
    $.ajax('galaria/function_controller.php', { type: "post",
        data: {
            function: 'get_reservation_type'
        }
    })
    .then(
        function success(result) {
                if(isJSON(result) != 0)
                {
                var data = JSON.parse(result);
                removeOptions(document.getElementById("nr_reservation_type"));

                nr_reservation_type

                for(var i=0; i<data.length; i++)
                {
                    
                    addOptions(document.getElementById("nr_reservation_type") , data[i]['type'] , data[i]['id']);
                    
                }
        
                $('#nr_reservation_type').selectpicker('refresh');


                        $.ajax('galaria/function_controller.php', { type: "post",
                        data: {
                            function: 'get_services'
                        }
                        })
                        .then(
                            function success(result) {
                                if(isJSON(result) != 0)
                                {
                                    var data = JSON.parse(result); 
                                    var div_content = '';

                                    for(var i=0; i<data.length; i++)
                                    {
                                        div_content += 
                                        '\
                                        <label class="m-checkbox">\
                                        <input type="checkbox" name="nr_services" value="'+data[i]['id']+'" onClick="calc_price();">\
                                        <div class="row" style="width: 160%;">\
                                        <div class="col-md-8">'+data[i]['title']+'</div><div class="col-md-4" style="text-align: right;"><b>'+data[i]['price']+'€</b></div>\
                                        </div>\
                                        <span></span>\
                                        </label>';
                                    }
                                    document.getElementById("nr_services_div").innerHTML=div_content;

                                    page_load_hide();
                                    $('#new_reservation_modal').modal('show');
                                }
                                else
                                {
                                    console.error('Request "' + init_new_reservation.name +'" failed. \nNot json return'+'\nreturn : ' + result);
                                    page_load_hide();
                                    $('#new_reservation_modal').modal('show');
                                }
                            },
                    
                            function fail(data) {
                                console.error('Request "' + init_new_reservation.name +'" failed. \nReturned status of "' + data.statusText + '" ' + data.status +'\nreturn : ' + data.responseText);
                                page_load_hide();
                            }
                        );
                }
                else
                {
                        console.error('Request "' + init_new_reservation.name +'" failed. \nNot json return'+'\nreturn : ' + result);
                        page_load_hide();
                }

        },

        function fail(data) {
          console.error('Request "' + init_new_reservation.name +'" failed. \nReturned status of "' + data.statusText + '" ' + data.status +'\nreturn : ' + data.responseText);
          page_load_hide();
        }
    );



}


