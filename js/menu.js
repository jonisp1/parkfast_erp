


function menu_info_load()
{
    $.ajax('galaria/function_controller.php', { type: "post",
        data: {
            function: 'get_menu_info'
        }
    })
    .then(
        function success(result) {
            if(isJSON(result) != 0)
            { 
                var ret = JSON.parse(result); 
                document.getElementById("top_menu_name").innerHTML=ret[0]['username'];
                document.getElementById("top_menu_email").innerHTML=ret[0]['email'];
            }
            else
            {
                console.error('Request "' + menu_info_load.name +'" failed. \nNot json return'+'\nreturn : ' + result);
                modal_loader_hide("new_reservation_modal");
            }
        },

        function fail(data) {
          console.error('Request "' + menu_info_load.name +'" failed. \nReturned status of "' + data.statusText + '" ' + data.status +'\nreturn : ' + data.responseText);
        }
    );
}


function logout_button()
{
    $.ajax('galaria/function_controller.php', { type: "post",
        data: {
            function: 'logout'
        }
    })
    .then(
        function success(result) {
            location.reload(); 
        },

        function fail(data) {
          console.error('Request "' + logout_button.name +'" failed. \nReturned status of "' + data.statusText + '" ' + data.status +'\nreturn : ' + data.responseText);
        }
    );
}


