<?php



error_reporting(E_ALL);
ini_set('display_errors', 1);

function get_parking_time($check_in , $check_out , $tolerance)
{
    $period_h = round((strtotime($check_out) - strtotime($check_in))/60, 2);   //sinolo lepta pou tha parkarei           
    $period_h =$period_h - $tolerance;
   

    $time = array();
    $time['days'] = 0;
    $time['hours'] = 0;
    $time['minutes'] = 0;
    
    $time['true_hours'] = 0;
    $time['true_days'] = 0;


    while($period_h > 0)
    {
        if($period_h >= 1440) //meres
        {
            $time['days']++;
            $period_h = $period_h - 1440;
        }
        else if($period_h >= 60)//lepta
        {
            $time['hours']++;
            $period_h = $period_h - 60;
        }
        else if($period_h > 0)
        {
            $time['minutes'] = intval($period_h);
            $period_h = 0;
        }
    }

    //total true hours
    
    if($time['days'] > 0)
          $time['true_hours'] = ($time['days'] * 24);
    
    $time['true_hours'] += $time['hours'];
    if($time['minutes'] > 1)
    {
        $time['true_hours']++;
    }

    $temp_h = $time['hours'];
    $time['true_days'] = $time['days'];
    if($time['minutes'] > 1)
    {
        $temp_h++;
    }
    if($temp_h > 1)
    {
        $time['true_days']++;
    }
    

    if($time['true_hours'] == 0)   
        $time['true_hours'] =1;
    if($time['true_days'] == 0)
        $time['true_days'] =1;

    return  $time;
}


function get_service_price($input_service , $park_time , $charge_type)
{
    include 'connect_medoo.php';
    
    
    $price = 0;

    $datas = $database->select("services", '*');
    $error = $database->error();

    if($error[0] = '00000') //all ok
    {
        for($i =0; $i < count($input_service); $i++)
        {
            for($j =0; $j < count($datas); $j++)
            {
                if($datas[$j]['id'] == $input_service[$i])
                {
                    if($datas[$j]['pay_type'] == 1) //xronoxreosh
                    {
                       if($charge_type == 1) //wra
                       {
                           if($datas[$j]['max_charge'] > 0)
                           {
                                if($park_time['true_hours'] > $datas[$j]['max_charge'])
                                    $price +=(float) $datas[$j]['price'] * $datas[$j]['max_charge'];
                                else
                                    $price +=(float) $datas[$j]['price'] * $park_time['true_hours'];
                           }
                           else
                           {
                                 $price +=(float) $datas[$j]['price'] * $park_time['true_hours'];
                           }
                       }
                       else
                       {
                            if($datas[$j]['max_charge'] > 0)
                            {
                                if($park_time['true_days'] > $datas[$j]['max_charge'])
                                    $price +=(float) $datas[$j]['price'] * $datas[$j]['max_charge'];
                                else
                                    $price +=(float) $datas[$j]['price'] * $park_time['true_days'];
                            }
                            else
                            {
                                 $price +=(float) $datas[$j]['price'] * $park_time['true_days'];
                            }
                       }
                    }
                    else 
                    {
                        $price +=(float) $datas[$j]['price'];
                    } 
                }
            }
         
        }
    }
    else
        header("HTTP/1.0 420 DB problem");

    return $price;
}
?>