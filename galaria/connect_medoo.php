<?php

require_once 'Medoo.php';

use Medoo\Medoo;



    $database = new Medoo([
        // required
        'database_type' => 'mysql',
        'database_name' => 'parkfast_erp',
        'server' => 'localhost',
        'username' => 'root',
        'password' => '',
        
        // [optional]
        'charset' => 'utf8',
        'port' => 3306,
        
        
        // [optional] Enable logging (Logging is disabled by default for better performance)
        'logging' => true,
        
        
    ]);






?>